# readmore

#### introduction
readmore，expand text，add expand / collapse text in the end of the text if the text is too long.
tip, show the promot when hove over the content

#### How to use it
1. npm i @colinxu91/readmore

2. import module and install in Vue，reference to examples/main.js
 

```
import readmore from '../packages/index';
app.use(readmore);
```


3. properties

3.1 readmore

| property  | coment                                                                                                 |
|-----------|--------------------------------------------------------------------------------------------------------|
| text      | whole text                                                                                             |
| moreText  | promot text for expand when whole text is too long                                                    |
| lessText  | promot text for collapse when whole text is too long                                                  |
| hideType  | collapse rule，according to character length - CHAR，according to words - WORD，according to lines - LINE |
| maxLength | max length to display in collapse mode                                                                 |
| isImage   | The less / more text is image path                                                                 |

3.2 tip
| property  | coment                                                                                                 |
|-----------|--------------------------------------------------------------------------------------------------------|
| content      | whole text                                                                                             |
| tip  | the text when hove over contnent                                                    |

**You can use slot to create your own content and tip template**

4. example

4.1 readmore

```
<readmore
  class='readmore-example'
  :text="'This is Text Example, you can replace it as any text as you want, and as long as you want.'"
  :hideType="'LINE'"
  :maxLength="2"
  :moreText="'▼'"
  :lessText="'▲'">
</readmore>
.readmore-example {
  width: 100px;
}
```

```
<readmore
  class='readmore-example'
  :text="'This is Text Example, you can replace it as any text as you want, and as long as you want.'"
  :hideType="'LINE'"
  :maxLength="2"
  :moreText="'/expand.png'"
  :lessText="'/collapse.png'"
  :isImage="true">
</readmore>
.readmore-example {
  width: 100px;
}
```

4.2 tip

```
<tip class="tip" 
  :content="'This is Text Example, you can replace it as any text as you want, and as long as you want.'"
  :tip="'This is Text Example, you can replace it as any text as you want, and as long as you want.'"></tip>
```

```
<tip>
    <template v-slot:content>
      <div>Content Slot Test</div>
    </template>
    <template v-slot:tip>
      <div>Tip Slot Test</div>
    </template>
  </tip>
```

