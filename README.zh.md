# readmore

#### 介绍
更多，文本展开， 文本过长在文本末添加展开/收起的按钮的组件
浮动提示，鼠标移入时显示提示内容

#### 使用说明
1. npm i @colinxu91/readmore

2. vue 引入 module和install， 参照examples里面的main.js
 

```
import readmore from '../packages/index';
app.use(readmore);
```


3. 属性说明

3.1 readmore
| 属性        | 说明                            |
|-----------|-------------------------------|
| text      | 完整文本                          |
| moreText  | 文本过长，需要展开时的提示文本               |
| lessText  | 文本过长，展开后需要隐藏时的提示文本            |
| hideType  | 隐藏规则，字符长度CHAR，单词个数WORD，行数LINE |
| maxLength | 最大长度                          |
| isImage   | 是否使用图片作为提示标签           |

3.2 tip
| property  | coment                                                                                                 |
|-----------|--------------------------------------------------------------------------------------------------------|
| content      | 显示内容                                                                                             |
| tip  | 鼠标移入时显示的提示内容                                                    |
**可以使用slot来创建content和tip模板**

4. 例子


```
<readmore
  class='readmore-example'
  :text="'This is Text Example, you can replace it as any text as you want, and as long as you want.'"
  :hideType="'LINE'"
  :maxLength="2"
  :moreText="'▼'"
  :lessText="'▲'">
</readmore>
.readmore-example {
  width: 100px;
}
```

```
<readmore
  class='readmore-example'
  :text="'This is Text Example, you can replace it as any text as you want, and as long as you want.'"
  :hideType="'LINE'"
  :maxLength="2"
  :moreText="'/expand.png'"
  :lessText="'/collapse.png'"
  :isImage="true">
</readmore>
.readmore-example {
  width: 100px;
}
```

4.2 tip

```
<tip class="tip" 
  :content="'This is Text Example, you can replace it as any text as you want, and as long as you want.'"
  :tip="'This is Text Example, you can replace it as any text as you want, and as long as you want.'"></tip>
```

```
<tip>
    <template v-slot:content>
      <div>Content Slot Test</div>
    </template>
    <template v-slot:tip>
      <div>Tip Slot Test</div>
    </template>
  </tip>
```