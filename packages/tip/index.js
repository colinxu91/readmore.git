// 导入组件，组件必须声明 name
import tip from './src/tip.vue'

// 为组件提供 install 安装方法，供按需引入
tip.install = (Vue) => {
  Vue.component(tip.name, tip)
}

// 默认导出组件
export default tip
