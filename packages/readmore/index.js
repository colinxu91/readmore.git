// 导入组件，组件必须声明 name
import readmore from './src/readmore.vue'

// 为组件提供 install 安装方法，供按需引入
readmore.install = (Vue) => {
  Vue.component(readmore.name, readmore)
}

// 默认导出组件
export default readmore
